tgNamespace.tgContainer.registerInnerFuncLib("wt", function() {

    // Register Inner Gate Function
    this.funcWTCDB.master.registerInnerGateFunctions("wt", "userdata", this.funcWTCDB.userdata);

    // Load Library
    this.funcWTCDB.loadLibrary();
},
function(oWrapper, oLocalConf){

    this.funcWTCDB = {};
    this.funcWTCDB.master = oWrapper;
    this.funcWTCDB.conf = oLocalConf;

    this.funcWTCDB.modifyAddressElement = function(sElement)
    {
        sElement = sElement.toLowerCase();
        sElement = sElement.replace(/\u00e4/g, "ae");
        sElement = sElement.replace(/\u00f6/g, "oe");
        sElement = sElement.replace(/\u00fc/g, "ue");
        sElement = sElement.replace(/\u00df/g, "ss");
        sElement = sElement.replace(/(\s|-|_)/g,"");
        return sElement;
    };

    this.funcWTCDB.modifyPhoneElement = function(sElement)
    {
        sElement = sElement.toLowerCase();
        sElement = sElement.match(/\d/g);
        sElement = sElement.join("");
        sElement = sElement.replace(/^0+/, '');
        return sElement;
    };

    this.funcWTCDB.userdata = function(oArgs)
    {
        if (this.funcWTCDB.master.debug("wt", "funcWTCDB_userdata"))debugger;

        // E-Mail
        if(oArgs.email !== "anonymous")
        {
            var sEmail = oArgs.email;

            // Alle Zeichen in Kleinbuchstaben umwandeln + Alle Leerzeichen entfernen
            sEmail = sEmail.toLowerCase().trim();

            window.wt_cdbData = window.wt_cdbData || [];
            window.wt_cdbData.push([
                "email",
                this.funcWTCDB.master.getSHA256Hash(sEmail),
                "sha256"
            ]);
        }

        // Rechnungsadresse
        if(
            oArgs.oPaymentAddress !== "undefined" &&
            oArgs.oPaymentAddress !== false
        )
        {
            var sPaymentAddress = (
                this.funcWTCDB.modifyAddressElement(oArgs.oPaymentAddress.firstname) + "|" +
                this.funcWTCDB.modifyAddressElement(oArgs.oPaymentAddress.lastname) + "|" +
                this.funcWTCDB.modifyAddressElement(oArgs.oPaymentAddress.postalcode) + "|" +
                this.funcWTCDB.modifyAddressElement(oArgs.oPaymentAddress.street) + "|" +
                this.funcWTCDB.modifyAddressElement(oArgs.oPaymentAddress.streetNumber)
            );

            window.wt_cdbData = window.wt_cdbData || [];
            window.wt_cdbData.push([
                "address",
                this.funcWTCDB.master.getSHA256Hash(sPaymentAddress),
                "sha256"
            ]);

            // Telefonnummer
            if(
                typeof oArgs.oPaymentAddress.phone1 !== "undefined" &&
                oArgs.oPaymentAddress.phone1 !== null &&
                oArgs.oPaymentAddress.phone1.length > 0
            )
            {
                var sTelephoneNumber = this.funcWTCDB.modifyPhoneElement(oArgs.oPaymentAddress.phone1);

                window.wt_cdbData = window.wt_cdbData || [];
                window.wt_cdbData.push([
                    "phone",
                    this.funcWTCDB.master.getSHA256Hash(sTelephoneNumber),
                    "sha256"
                ]);
            }
            else if
            (
                typeof oArgs.oPaymentAddress.phone2 !== "undefined" &&
                oArgs.oPaymentAddress.phone2 !== null &&
                oArgs.oPaymentAddress.phone2.length > 0
            )
            {
                var sTelephoneNumber = this.funcWTCDB.modifyPhoneElement(oArgs.oPaymentAddress.phone2);
                window.wt_cdbData = window.wt_cdbData || [];
                window.wt_cdbData.push([
                    "phone",
                    this.funcWTCDB.master.getSHA256Hash(sTelephoneNumber),
                    "sha256"
                ]);
            }
        }
    };

    this.funcWTCDB.loadLibrary = function()
    {
        var self = this;
        window.wt_cdb = function(conf) {
            if(conf.mode === "page" && conf.type === "before" && conf.requestCounter === 1) {
                var cdbConfig = window.cdbConfig || {};

                /* W18-START */
                if(typeof self.conf.oCDB !== "undefined")
                {
                    if(typeof self.conf.oCDB.oAdClear !== "undefined")
                    {
                        if(typeof self.conf.oCDB.oAdClear.readCriteoXDeviceId !== "undefined")
                        {
                            cdbConfig.readCriteoXDeviceId = self.conf.oCDB.oAdClear.readCriteoXDeviceId;
                        }

                        if(typeof self.conf.oCDB.oAdClear.adClearTrackDomain !== "undefined")
                        {
                            cdbConfig.adClearTrackDomain = self.conf.oCDB.oAdClear.adClearTrackDomain;
                        }

                        if(typeof self.conf.oCDB.oAdClear.adClearTrackId !== "undefined")
                        {
                            cdbConfig.adClearTrackId = self.conf.oCDB.oAdClear.adClearTrackId;
                        }
                    }
                }
                /* W18-ENDE */

                if(typeof conf.instance.cdbData === "object") {
                    var Crypto=Crypto||{};Crypto.URL=function(){this.encode=function(h){h+="";try{return encodeURIComponent(h)}catch(r){return escape(h)}};this.decode=function(h){h+="";try{return decodeURIComponent(h)}catch(r){return unescape(h)}}};Crypto=Crypto||{};
                    Crypto.MD5=function(h){var r=0,n=h?h:!1,l=function(e,b,a,d,c,f){e=p(p(b,e),p(d,f));return p(e<<c|e>>>32-c,a)},g=function(e,b,a,d,c,f,g){return l(b&a|~b&d,e,b,c,f,g)},m=function(e,b,a,d,c,f,g){return l(b&d|a&~d,e,b,c,f,g)},k=function(e,b,a,d,c,f,g){return l(a^(b|~d),e,b,c,f,g)},p=function(e,b){var a=(e&65535)+(b&65535);return(e>>16)+(b>>16)+(a>>16)<<16|a&65535};this.encode=function(e){e+="";n&&(e=(new Crypto.URL).encode(e));for(var b="",a=-1,d=e.length,c,f;++a<d;)c=e.charCodeAt(a),f=a+1<d?e.charCodeAt(a+
                        1):0,55296<=c&&(56319>=c&&56320<=f&&57343>=f)&&(c=65536+((c&1023)<<10)+(f&1023),a++),127>=c?b+=String.fromCharCode(c):2047>=c?b+=String.fromCharCode(192|c>>>6&31,128|c&63):65535>=c?b+=String.fromCharCode(224|c>>>12&15,128|c>>>6&63,128|c&63):2097151>=c&&(b+=String.fromCharCode(240|c>>>18&7,128|c>>>12&63,128|c>>>6&63,128|c&63));a=b.length;e=[a>>2];d=e.length;for(c=0;c<d;c++)e[c]=0;for(d=0;d<8*a;d+=8)e[d>>5]|=(b.charCodeAt(d/8)&255)<<d%32;b=8*b.length;e[b>>5]|=128<<b%32;e[(b+64>>>9<<4)+14]=b;b=1732584193;
                        a=-271733879;d=-1732584194;c=271733878;for(f=0;f<e.length;f+=16){var h=b,s=a,q=d,u=c,b=g(b,a,d,c,e[f+0],7,-680876936);c=g(c,b,a,d,e[f+1],12,-389564586);d=g(d,c,b,a,e[f+2],17,606105819);a=g(a,d,c,b,e[f+3],22,-1044525330);b=g(b,a,d,c,e[f+4],7,-176418897);c=g(c,b,a,d,e[f+5],12,1200080426);d=g(d,c,b,a,e[f+6],17,-1473231341);a=g(a,d,c,b,e[f+7],22,-45705983);b=g(b,a,d,c,e[f+8],7,1770035416);c=g(c,b,a,d,e[f+9],12,-1958414417);d=g(d,c,b,a,e[f+10],17,-42063);a=g(a,d,c,b,e[f+11],22,-1990404162);b=g(b,a,d,c,
                            e[f+12],7,1804603682);c=g(c,b,a,d,e[f+13],12,-40341101);d=g(d,c,b,a,e[f+14],17,-1502002290);a=g(a,d,c,b,e[f+15],22,1236535329);b=m(b,a,d,c,e[f+1],5,-165796510);c=m(c,b,a,d,e[f+6],9,-1069501632);d=m(d,c,b,a,e[f+11],14,643717713);a=m(a,d,c,b,e[f+0],20,-373897302);b=m(b,a,d,c,e[f+5],5,-701558691);c=m(c,b,a,d,e[f+10],9,38016083);d=m(d,c,b,a,e[f+15],14,-660478335);a=m(a,d,c,b,e[f+4],20,-405537848);b=m(b,a,d,c,e[f+9],5,568446438);c=m(c,b,a,d,e[f+14],9,-1019803690);d=m(d,c,b,a,e[f+3],14,-187363961);a=m(a,
                            d,c,b,e[f+8],20,1163531501);b=m(b,a,d,c,e[f+13],5,-1444681467);c=m(c,b,a,d,e[f+2],9,-51403784);d=m(d,c,b,a,e[f+7],14,1735328473);a=m(a,d,c,b,e[f+12],20,-1926607734);b=l(a^d^c,b,a,e[f+5],4,-378558);c=l(b^a^d,c,b,e[f+8],11,-2022574463);d=l(c^b^a,d,c,e[f+11],16,1839030562);a=l(d^c^b,a,d,e[f+14],23,-35309556);b=l(a^d^c,b,a,e[f+1],4,-1530992060);c=l(b^a^d,c,b,e[f+4],11,1272893353);d=l(c^b^a,d,c,e[f+7],16,-155497632);a=l(d^c^b,a,d,e[f+10],23,-1094730640);b=l(a^d^c,b,a,e[f+13],4,681279174);c=l(b^a^d,c,b,
                            e[f+0],11,-358537222);d=l(c^b^a,d,c,e[f+3],16,-722521979);a=l(d^c^b,a,d,e[f+6],23,76029189);b=l(a^d^c,b,a,e[f+9],4,-640364487);c=l(b^a^d,c,b,e[f+12],11,-421815835);d=l(c^b^a,d,c,e[f+15],16,530742520);a=l(d^c^b,a,d,e[f+2],23,-995338651);b=k(b,a,d,c,e[f+0],6,-198630844);c=k(c,b,a,d,e[f+7],10,1126891415);d=k(d,c,b,a,e[f+14],15,-1416354905);a=k(a,d,c,b,e[f+5],21,-57434055);b=k(b,a,d,c,e[f+12],6,1700485571);c=k(c,b,a,d,e[f+3],10,-1894986606);d=k(d,c,b,a,e[f+10],15,-1051523);a=k(a,d,c,b,e[f+1],21,-2054922799);
                            b=k(b,a,d,c,e[f+8],6,1873313359);c=k(c,b,a,d,e[f+15],10,-30611744);d=k(d,c,b,a,e[f+6],15,-1560198380);a=k(a,d,c,b,e[f+13],21,1309151649);b=k(b,a,d,c,e[f+4],6,-145523070);c=k(c,b,a,d,e[f+11],10,-1120210379);d=k(d,c,b,a,e[f+2],15,718787259);a=k(a,d,c,b,e[f+9],21,-343485551);b=p(b,h);a=p(a,s);d=p(d,q);c=p(c,u)}e=[b,a,d,c];b="";a=0;for(d=e.length;a<32*d;a+=8)b+=String.fromCharCode(e[a>>5]>>>a%32&255);e=b;if("undefined"===typeof r||null===r)r=0;b=r?"0123456789ABCDEF":"0123456789abcdef";a="";c=0;for(f=
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   e.length;c<f;c++)d=e.charCodeAt(c),a+=b.charAt(d>>>4&15)+b.charAt(d&15);return a}};Crypto=Crypto||{};
                    Crypto.SHA256=function(){var h=function(h,l){return l>>>h|l<<32-h},r=function(h){for(var l="",g,m=7;0<=m;m--)g=h>>>4*m&15,l+=g.toString(16);return l};this.encode=function(n){n=unescape(encodeURIComponent(n+""));var l=[1116352408,1899447441,3049323471,3921009573,961987163,1508970993,2453635748,2870763221,3624381080,310598401,607225278,1426881987,1925078388,2162078206,2614888103,3248222580,3835390401,4022224774,264347078,604807628,770255983,1249150122,1555081692,1996064986,2554220882,2821834349,2952996808,
                        3210313671,3336571891,3584528711,113926993,338241895,666307205,773529912,1294757372,1396182291,1695183700,1986661051,2177026350,2456956037,2730485921,2820302411,3259730800,3345764771,3516065817,3600352804,4094571909,275423344,430227734,506948616,659060556,883997877,958139571,1322822218,1537002063,1747873779,1955562222,2024104815,2227730452,2361852424,2428436474,2756734187,3204031479,3329325298],g=[1779033703,3144134277,1013904242,2773480762,1359893119,2600822924,528734635,1541459225];n+=String.fromCharCode(128);
                        var m=Math.ceil((n.length/4+2)/16),k=Array(m),p;for(p=0;p<m;p++){k[p]=Array(16);for(var e=0;16>e;e++)k[p][e]=n.charCodeAt(64*p+4*e)<<24|n.charCodeAt(64*p+4*e+1)<<16|n.charCodeAt(64*p+4*e+2)<<8|n.charCodeAt(64*p+4*e+3)}k[m-1][14]=8*(n.length-1)/Math.pow(2,32);k[m-1][14]=Math.floor(k[m-1][14]);k[m-1][15]=8*(n.length-1)&4294967295;n=Array(64);var b,a,d,c,f,t,s,q;for(p=0;p<m;p++){for(q=0;16>q;q++)n[q]=k[p][q];for(q=16;64>q;q++)n[q]=(h(17,n[q-2])^h(19,n[q-2])^n[q-2]>>>10)+n[q-7]+(h(7,n[q-15])^h(18,n[q-
                        15])^n[q-15]>>>3)+n[q-16]&4294967295;e=g[0];b=g[1];a=g[2];d=g[3];c=g[4];f=g[5];t=g[6];s=g[7];for(q=0;64>q;q++){var u=s+(h(6,c)^h(11,c)^h(25,c))+(c&f^~c&t)+l[q]+n[q],v=(h(2,e)^h(13,e)^h(22,e))+(e&b^e&a^b&a);s=t;t=f;f=c;c=d+u&4294967295;d=a;a=b;b=e;e=u+v&4294967295}g[0]=g[0]+e&4294967295;g[1]=g[1]+b&4294967295;g[2]=g[2]+a&4294967295;g[3]=g[3]+d&4294967295;g[4]=g[4]+c&4294967295;g[5]=g[5]+f&4294967295;g[6]=g[6]+t&4294967295;g[7]=g[7]+s&4294967295}return r(g[0])+r(g[1])+r(g[2])+r(g[3])+r(g[4])+r(g[5])+
                            r(g[6])+r(g[7])}};
                    (function(h,r,n){var l={},g=[];window.wt_cdbData=window.wt_cdbData||[];var m=function(a,d){a=a.toLowerCase();d&&(a=a.replace(d,""));return a},k=function(a,d,c){if("undefined"===typeof g[a]||"string"===typeof g[a]&&"1"!==g[a]){var b=d;c||(b=m(d));l[a]=b;g[a]=b;h.setCookie("wt_cdb",g.slice(1).join(","))}},p=function(a){var d=a[1],c="undefined"!==typeof a[2]?a[2]:"";switch(a[0]){case "email":a=d;c||(a=m(a,/\s/g));switch(c){case "md5":k(1,a);break;case "sha256":k(2,a);break;default:k(1,r.encode(a)),k(2,
                        n.encode(a))}break;case "phone":a=d;c||(a=m(a,/(\s|\D)/g));switch(c){case "md5":k(3,a);break;case "sha256":k(4,a);break;default:k(3,r.encode(a)),k(4,n.encode(a))}break;case "address":a=d;if(!c){a=m(a);for(var d=[[/\u00e4/g,"ae"],[/\u00f6/g,"oe"],[/\u00fc/g,"ue"],[/\u00df/g,"ss"],[/[\s_\-]/g,""],[/str(\.)?(\s|\|)/g,"strasse|"]],b=0;b<d.length;b++)a=a.replace(d[b][0],d[b][1])}switch(c){case "md5":k(5,a);break;case "sha256":k(6,a);break;default:k(5,r.encode(a)),k(6,n.encode(a))}break;case "android":k(7,
                        d);break;case "ios":k(8,d);break;case "windows":k(9,d);break;case "facebook":k(10,n.encode(m(d)));break;case "twitter":k(11,n.encode(m(d)));break;case "google":k(12,n.encode(m(d)));break;case "linkedIn":k(13,n.encode(m(d)));break;case "criteo":k(14,d,!0);break;case "amp":k(15,d,!0);break;case "adclear":k(16,d,!0);break;case "custom":"number"===typeof c&&(50<c&&80>c)&&k(c,d,!0)}},e=function(){var a=h.getCookie("cto_axid");a?window.wt_cdbData.push(["criteo",a]):(a=h.urlParam(document.location.href,
                        "wt_ccdid",!1))&&window.wt_cdbData.push(["criteo",a])},b=function(){var a=h.getCookie("wt_acv_id");a?"1"!==a&&window.wt_cdbData.push(["adclear",a]):h.loadAsynchron("//"+cdbConfig.adClearTrackDomain+"/acv/"+cdbConfig.adClearTrackId+"/ckid","adclearCookieId",function(){"undefined"===typeof window.adclearCookieId?h.setCookie("wt_acv_id","1"):(h.setCookie("wt_acv_id",window.adclearCookieId),window.wt_cdbData.push(["adclear",window.adclearCookieId]))},2E3)};(function(){g=h.getCookie("wt_cdb").split(",");
                        g.unshift("");for(var a=0;a<g.length;a++)g[a]&&"1"!==g[a]&&(l[a]=g[a])})();(function(){for(var a=0;a<window.wt_cdbData.length;a++)p(window.wt_cdbData[a]);window.wt_cdbData={push:function(a){p(a)},get:function(a){return"undefined"!==typeof l[a]?l[a]:""},length:0,version:"2.0.4"};h.cdbData=window.wt_cdbData})();(function(){var a=h.getCookie("amp-wt3-eid");a&&window.wt_cdbData.push(["amp",a])})();cdbConfig.adClearTrackDomain&&cdbConfig.adClearTrackId&&b();cdbConfig.readCriteoXDeviceId&&e();(function(){h.cdbSendInterval=
                        window.setInterval(function(){var a="//"+h.trackDomain+"/"+h.trackId+"/cdb?p="+h.version+",0&v=2.0.4",d=!1;h.eid&&(a+="&eid="+h.eid);for(var c in l){var b=c+"";"string"===typeof l[b]&&""!==l[b]&&(a+="&cdb"+b+"="+h.wtEscape(l[b]),g[b]="1",d=!0)}d&&(l={},h.setCookie("wt_cdb",g.slice(1).join(",")),h.sendPixel(a,"cdb"))},2500)})()})(conf.instance,new Crypto.MD5,new Crypto.SHA256);

                }
            }
        };
    };
});
tgNamespace.tgContainer.registerInnerConfig(
    "wt",
    //LIVE CONF
    {
        countConfigs: 2,
        countFuncLibs: 3,
        countPlugins: 0,
        getContentIdByURL: function ()
        {
            var url = document.location.href;
            if (url && url !== null)
            {
                return url.split("?")[0].toLowerCase();
            }
            return "no_content";
        },
        oWebtrekkConfig: {
            contentId: function ()
            {
                var url = document.location.href;
                if (url && url !== null)
                {
                    return url.split("?")[0].toLowerCase();
                }
                return "no_content";
            },
            cookie: "1",
            executePluginFunction: "wt_socialMedia;wt_teaserTracking;wt_cdb",
            execCDB: true,
            allowedUrlParameters: [
                ""
            ]
        },
        iErrorPageParameterKey: 3,
        iSearchPageParameterKey: 1,
        iSaleProductBundlesEcommerceParameterKey: 1,
        iEventClickParameterKeyGroup:1,
        iEventClickParameterKeyValue:2,
        iEventClickParameterKeyNumValue:3,
        iEventClickParameterKeySearchSource:4,
        iEventClickParameterKeySearchType:5,
        iEventClickParameterKeySearchGoal:6,
        iEventClickParameterKeySearchTerm:7,
        iEventClickParameterKeySearchCountresults:8,
        iEventClickParameterOutLink:9,

        oEventClickParameterFilter : {
          "category": 10, // use the same name as in the trackgate filter object
          "employment": 11
        },

        aPageVarKeys: {
            "Subprojekt": 8,
            "Device": 7,
            "PageGroups": 3,
            "Seitenebene-1": 12,
            "Seitenebene-2": 13,
            "Seitenebene-3": 14,
            "auftragtype":5,
            "auftragsnummer":6,
            "protocol":15,
            "loginstatus" : 16,
            "searchsource" : 17,
            "searchtype" : 18,
            "searchgoal" : 19,
            "searchcountresults" : 20,
            "category": 10,
            "employment": 11
        },
        aSessionVarKeys: {
            "filiale" : 2,
            "deviceid" : 3
        },
        aUserSessionVarKeys: {
        },
        aGoalParIds: {},
        iUrlParameterPageParameterKey: 2,
        aBlackListParameter : [

        ],
        aBlackListValue : [
            "utm_id",
            "wt_mc",
            "tg_mc",
            "mc",
            "wt_kw01",
            "wt_kw02",
            "wt_kw03",
            "wt_kw04",
            "wt_kw05",
            "wt_kw06",
            "wt_kw07",
            "wt_kw08",
            "wt_kw09",
            "wt_kw10",
            "wt_ad01",
            "wt_ad02",
            "wt_ad03",
            "wt_ad04",
            "wt_ad05",
            "wt_ad06",
            "wt_ad07",
            "wt_ad08",
            "wt_ad09",
            "wt_ad10",
            "utm_source",
            "utm_medium",
            "utm_content",
            "utm_keyword",
            "utm_campaign",
            "gclid",
            "gcsrc",
            "utm_term"
        ],
        aWhiteListPageParameter : {
            "fh_search": 4
        },
        aContentGroup: {
            "Seitentyp-1": [1],
            "Seitentyp-2": [2]
        },
        aContentGroupVariable: {
            "Device": [7],          //<name der Variablen - muss unter aPageVarKeys konfiguriert seind > : <Array von den GroupenIDs die als mehrere Werte in die Variable getrackt werden sollen>
            "PageGroups": [1, 2, 3, 4, 5, 6, 9, 10],
            "Subprojekt" : [8],
            "Seitenebene-1": [1],
            "Seitenebene-2": [2],
            "Seitenebene-3": [3]
        },
        aProductGroupVariable: {
            "Seitenebene-1" : [1],
            "Seitenebene-2" : [2],
            "Seitenebene-3" : [3]
        },
        preloadFiles: [
            "//www.xxxlshop.de/static/nginx/default/js/webtrekk_v4_4_1.min.js"
        ],
        oSearchCountCluster: [
            {"min": 0, "max": 0},
            {"min": 1, "max": 10},
            {"min": 11, "max": 50},
            {"min": 51, "max": 200},
            {"min": 201, "max": 500},
            {"min": 501, "max": 1000}
        ],
        oSiteTimingCluster : [
            {"min": 0, "max": 0},
            {"min": 0, "max": 0.5},
            {"min": 0.5, "max": 1},
            {"min": 1, "max": 1.5},
            {"min": 1.5, "max": 2},
            {"min": 2, "max": 2.5},
            {"min": 2.5, "max": 3},
            {"min": 3, "max": 3.5},
            {"min": 3.5, "max": 4},
            {"min": 4, "max": 4.5},
            {"min": 4.5, "max": 5},
            {"min": 5, "max": 6},
            {"min": 6, "max": 7},
            {"min": 7, "max": 8},
            {"min": 8, "max": 9},
            {"min": 9, "max": 10},
            {"min": 10, "max": 12},
            {"min": 12, "max": 14},
            {"min": 14, "max": 16},
            {"min": 16, "max": 18},
            {"min": 18, "max": 20}
        ],
        sTeaserMediaCode: "mc",
        aECommerceVarKeys: {
            "zahlart":1,
            "vouchercode":2,
            "vouchervalue":3,
            "auftragIDs": 6,
            "cookieweicheCall_attrlanding": 8,
            "cookieweicheCall_attrthankyou": 9,
            "cookieweicheCall_attrlock": 7,
            "Seitenebene-1" : 13,
            "Seitenebene-2" : 14,
            "Seitenebene-3" : 15,
            "montage" : 16,
            "lieferindikator" : 17,
            "aktionskuerzel" : 18,
            "aktionstage" : 19,
            "aktuelle_bewertungen" : 20,
            "preisberechnen" : 22,
            "preisberechnen_value" : 23
        },
        aProductEcommerceVarKeys:{
            "isOnlineOnly" : 11
        },
        iEcommerceParameterProductViewProductName: 5,
        iEcommerceParameterProductOldBrutto: 4,
        iEcommerceParameterProductBrand : 21,
        ProductEcommerceLowercaseKeys : ["lieferindikator","availableStatus"]
    },
    //TEST CONF
    {}
);

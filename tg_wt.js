tgNamespace.tgContainer.registerInnerGate("wt", function (oWrapper, oLocalConfData)
{
    this.oWrapper = oWrapper;
    this.oLocalConfData = oLocalConfData;
    this.oProducts = {};

    // Temp-Speicher für weitere Verwendung
    this.aPageVarsCache = {};
    this.aPageVarsPVCache = {};
    this.bProductView = false;
    this.sVirtualUrl = "";
    this.oCacheEcommerce = {};
    this.oProductEcommerce = {};
    this.sOrderID = false;
    this.sCWPartner = false;

    this._construct = function ()
    {
        if(this.oWrapper.debug("wt","_construct"))debugger;

        this.initial();
        this.oWrapper.registerInnerGateFunctions("wt", "pageview", this.trackPageView);
        //this.oWrapper.registerInnerGateFunctions("wt", "error", this.trackError);
        this.oWrapper.registerInnerGateFunctions("wt", "addtocart", this.addToCart);
        this.oWrapper.registerInnerGateFunctions("wt", "deletefromcart", this.deleteFromCart);
        this.oWrapper.registerInnerGateFunctions("wt", "!!!trackSaleProduct", this.trackSaleProduct);
        this.oWrapper.registerInnerGateFunctions("wt", "sale", this.trackSale);
        this.oWrapper.registerInnerGateFunctions("wt", "productview", this.trackProductView);
        this.oWrapper.registerInnerGateFunctions("wt", "teaser", this.trackTeaser);
        this.oWrapper.registerInnerGateFunctions("wt", "final", this.submit);
        this.oWrapper.registerInnerGateFunctions("wt", "event", this.trackEvent);
        this.oWrapper.registerInnerGateFunctions("wt", "sessionvar", this.trackSessionVar);
        this.oWrapper.registerInnerGateFunctions("wt", "pagevar", this.trackPageVar);
        this.oWrapper.registerInnerGateFunctions("wt", "uservar", this.trackUserVar);
        this.oWrapper.registerInnerGateFunctions("wt", "goal", this.trackGoal);
        this.oWrapper.registerInnerGateFunctions("wt", "outlink", this.trackOutLink);
        this.oWrapper.registerInnerGateFunctions("wt", "ecommercevar", this.trackECommerceVar);
        this.oWrapper.registerInnerGateFunctions("wt", "userid", this.trackUserID);
        this.oWrapper.registerInnerGateFunctions("wt", "campaign", this.trackCampaign);
        this.oWrapper.registerInnerGateFunctions("wt", "product", this.trackproduct);
        this.oWrapper.registerInnerGateFunctions("wt", "userdata", this.trackUserData);
        this.oWrapper.registerInnerGateFunctions("wt", "autoFormInit", this.autoFormTracking_Init);
        this.oWrapper.registerInnerGateFunctions("wt", "recommendationClick", this.trackRecommendationClick);
        this.oWrapper.registerInnerGateFunctions("wt", "search", this.trackSearch);

        if(typeof this.oLocalConfData.bS2STracking !== "undefined" && this.oLocalConfData.bS2STracking === true)
        {
            this.oWrapper.registerInnerGateFunctions("wt", "cookieweicheConversion", this.cookieweicheConversion);
        }

        //TODO: NOCH NICHT IN DER BRIDGE
        this.oWrapper.registerInnerGateFunctions("wt", "teaserview", this.trackTeaserView);
        this.oWrapper.registerInnerGateFunctions("wt", "form", this.trackForm);

    };
    this.initial = function ()
    {
        if(this.oWrapper.debug("wt","initial"))debugger;

        if(typeof this.wtsend === "undefined")
        {
            var webtrekk = this.getPreInitObject();
            window["webtrekkConfig"] = this.oLocalConfData.oWebtrekkConfig;

            this.wtsend = new webtrekkV3(webtrekk);
        }

        this.wt = {};

        // PageVarCache
        this.aPageVarsPVCache = {};
        this.bProductView = false;
        this.sOrderID = false;
        this.sCWPartner = false;
    };

    this.getPreInitObject = function ()
    {
        if(this.oWrapper.debug("wt","getPreInitObject"))debugger;

        var xUrlHeatmapTracking = this.oLocalConfData["sUrlHeatmapTracking"];
        var xUrlLinkTracking = this.oLocalConfData["sUrlLinkTracking"];
        var sUrl = window.location.hostname + window.location.pathname;

        var webtrekk = new Object;
        webtrekk.contentId =  this.oLocalConfData.getContentIdByURL();

        // load executePluginFunction from config
        var oLoadPluginFunction = this.oLocalConfData["loadPluginFunction"];

        if(typeof oLoadPluginFunction !== "undefined" && typeof oLoadPluginFunction === "object")
        {
            webtrekk["executePluginFunction"] = Object.keys(oLoadPluginFunction).join(";");
        }

        if(typeof xUrlHeatmapTracking !== "undefined" && xUrlHeatmapTracking !== null)
        {
            if(xUrlHeatmapTracking.test(sUrl))
            {
                webtrekk.heatmap = "1";

                // Refpoint setzen
                var sHeatmapRefPointID = this.oLocalConfData["sHeatmapRefPointID"];
                if(typeof sHeatmapRefPointID !== "undefined" && sHeatmapRefPointID !== null) {
                    webtrekk.heatmapRefpoint = sHeatmapRefPointID;
                }
            }
        }

        if(typeof xUrlLinkTracking !== "undefined" && xUrlLinkTracking !== null)
        {
            if (xUrlLinkTracking === true)
            {
                webtrekk["linkTrack"] = "standard";
            }
            else if(xUrlLinkTracking.test(sUrl))
            {
                webtrekk["linkTrack"] = "standard";
            }
        }

        return webtrekk;
    };

    this.submit = function ()
    {
        if(this.oWrapper.debug("wt","submit"))debugger;

        // PageVarCache speichern
        if(this.bProductView == true) {
            this.aPageVarsCache = this.aPageVarsPVCache;
        }

        var bSendPixel = true;
        if(typeof this.oLocalConfData.ignorePageviews !== "undefined")
        {
            for(var i in this.oLocalConfData.ignorePageviews)
            {
                if(this.oLocalConfData.ignorePageviews[i].test(this.wt.contentId) === true)
                {
                    //Aktuelle Seite steht auf der ignore Liste.
                    bSendPixel = false;
                }
            }
        }

        if(bSendPixel)
        {
            //Pixel wird abgeschickt.
            this.sendPixel();
        }
        else
        {
            //Auch wenn nicht abgeschickt wird, soll alles wieder initialisiert werden.
            this.initial();
        }
    };

    this.sendPixel = function (bInitial)
    {
        if(this.oWrapper.debug("wt","sendPixel"))debugger;

        this.wtsend.sendinfo(this.wt);
        //console.log(this.wt);

        if(bInitial !== false)
        {
            this.initial();
        }
    }
});

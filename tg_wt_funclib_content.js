
tgNamespace.tgContainer.registerInnerFuncLib(
    "wt", {
        "autoFormTracking_Init" : function(oArgs)
        {
            if(typeof $ !== "undefined")
            {
                var oConf = oArgs.conf;
                var sSelector = oArgs.selector;
                var $Form = $(sSelector);

                var sTempContentID = this.wtsend.contentId;
                this.wtsend.contentId = this.sVirtualUrl + ":" + $Form.attr("id");

                this.wtsend.formAttribute = "thisAttributeDoesNotExist";
                this.wtsend.formTrackInstall($Form[0]);

                this.wtsend.contentId = sTempContentID;
            }
        },
        "trackOutLink": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackOutLink"))debugger;

            var sUrl = oArgs.url;
            var sText = oArgs.text;

            var sPosition = "";
            if(typeof oArgs.position !== "undefined" && oArgs.position !== null)
            {
                sPosition = oArgs.position.toString();
            }

            var iParameterOutLink = this.oLocalConfData["iEventClickParameterOutLink"];
            var iParameterKeyGroup = this.oLocalConfData["iEventClickParameterKeyGroup"];
            var iParameterKeyValue = this.oLocalConfData["iEventClickParameterKeyValue"];

            this.wt["linkId"] = sText;
            this.wt["customClickParameter"] = new Object;
            this.wt["customClickParameter"][iParameterOutLink] = sUrl;
            this.wt["customClickParameter"][iParameterKeyGroup] = "OutgoingLink";
            this.wt["customClickParameter"][iParameterKeyValue] = sPosition;
            this.sendPixel();
        },
        "trackCampaign": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackCampaign"))debugger;

            var sUrlParameter = oArgs.queryparam;
            var sUrlValue = oArgs.queryvalue;

            this.wt.campaignId = sUrlParameter + "%3D" + sUrlValue;

        },
        "trackGoal": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackGoal"))debugger;

            var sGoalId = oArgs.id;

            var sParameterKey = this.oLocalConfData["aGoalParIds"][sGoalId];
            if (this.wt.customEcommerceParameter === false)
            {
                this.wt.customEcommerceParameter = {};
            }
            this.wt.customEcommerceParameter[sParameterKey] = sGoalId;

            this.wt.linkId = "goal-" + sGoalId;
            this.wt.customEcommerceParameter = {};
            this.wt.customEcommerceParameter[sParameterKey] = sGoalId;
            this.sendPixel();
        },
        "trackUserID": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackUserID"))debugger;

            var sUserID = oArgs.id;

            this.wt.customerId = sUserID;
        },
        "initUserVars" : function()
        {
            //Prüfen ob User Vars gesetzt sind.
            var sCookie = this.oWrapper.readCookie("w18_tg_wt_uservar");
            var oCookie = JSON.parse(sCookie);
            for(var sKey in oCookie)
            {
                this.trackUserVar({"key":sKey,"value":oCookie[sKey]});
            }
        },
        "trackPageView": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackPageView"))debugger;

            this.initUserVars();

            if(typeof oArgs.vurl !== "undefined")
            {
                var sVurl = oArgs.vurl;
            }
            else if(typeof oArgs.pagetitle !== "undefined" && oArgs.overwriteurl === true)
            {
                var sVurl = oArgs.pagetitle;
            }
            else
            {
                var sVurl = false;
            }

            if(typeof oArgs.pagegroup !== "undefined")
            {
                var oPageGroup = oArgs.pagegroup;
            }
            else
            {
                var oPageGroup = {};
            }
            
            // Track PageType as ContenGroups
            if(typeof oArgs.pagetype !== "undefined" && oArgs.pagetype !== null)
            {
                if(typeof oArgs.pagetype[0] !== "undefined" && typeof oArgs.pagetype[1] !== "undefined")
                {
                    this.trackContentGroup({key : "Seitentyp-1", value : oArgs.pagetype[0]});
                    this.trackContentGroup({key : "Seitentyp-2", value : oArgs.pagetype[1]});
                }
            }

            this.wt.mediaCode = "mc;tgmc";
            this.wt.mediaCodeCookie = "sid";

            var sURLParameterString = null;

            if (sVurl != false && sVurl.length > 0)
            {

                /*
                 Protokoll entfernen
                 */

                var aMatch = sVurl.match(/^https?:\/\//i);
                if(aMatch !== null && typeof aMatch === "object")
                {
                    // http & https
                    this.trackPageVar({
                        key : "protocol",
                        value : sVurl.substr(0, aMatch[0].length-3)
                    });

                    sVurl = sVurl.substr(aMatch[0].length, sVurl.length);
                }
                else
                {
                    if(!sVurl.match(/^.*\..*\..*/))
                    {
                        // x/a.php & /x/a.php
                        var sNewPageTitle = document.location.host;

                        if(sVurl.charAt(0) !== "/")
                        {
                            sNewPageTitle += "/";
                        }

                        sVurl = (sNewPageTitle + sVurl);
                    }

                    this.trackPageVar({
                        key : "protocol",
                        value : document.location.protocol.substr(0, document.location.protocol.length-1)
                    });
                }

                // Sind GET-Parameter vorhanden?
                if(sVurl.indexOf("?") !== -1)
                {
                    sURLParameterString = sVurl.substr(sVurl.indexOf("?"),sVurl.length);
                    sVurl = sVurl.substr(0,sVurl.indexOf("?"));
                }

                this.wt.contentId = sVurl;
                this.sVirtualUrl = sVurl;

            }
            else
            {
                var sContentID = document.location.host + document.location.pathname + "?";
                if(document.location.search.substr(1) !== "")
                {
                    var aUrlParameters = document.location.search.substr(1).split("&");
                    for (var i = 0; i < aUrlParameters.length; i++)
                    {
                        var sParameter = aUrlParameters[i];
                        if (sParameter.length > 0 && this.oLocalConfData.oWebtrekkConfig.allowedUrlParameters.indexOf(sParameter.split("=")[0]) > -1)
                        {
                            sContentID += sParameter + "&";
                        }
                    }
                }

                sContentID = sContentID.substr(0, sContentID.length - 1);
                this.wt.contentId = sContentID;
                this.sVirtualUrl = sContentID;

                this.trackPageVar({
                    key : "protocol",
                    value : document.location.protocol.substr(0, document.location.protocol.length-1)
                });

                // GET-Parameter als gesamten String auslesen

                sURLParameterString = document.location.href.split(/\?|\#/i);

            }

            // Content-ID für Events zwischenspeichern
            if(typeof this.wtsend !== "undefined")
            {
                this.wtsend.contentId = this.wt.contentId;
            }

            // PageVarKeys
            this.trackPageParameter(oPageGroup);

            var sParameterKey = this.oLocalConfData["iUrlParameterPageParameterKey"];
            if (sParameterKey !== false)
            {
                //gibt es url parameter?
                if (sURLParameterString !== null && sURLParameterString.length > 1)
                {
                    if (typeof this.wt.customParameter === "undefined")
                    {
                        this.wt.customParameter = {};
                    }

                    /* URL-Parameter Konzept */

                    sURLParameterString.splice(0,1);
                    var aURLParameter = [];

                    for(var i = 0; i<sURLParameterString.length; i++)
                    {
                        aURLParameter = aURLParameter.concat(sURLParameterString[i].split(/\&|\#/i));
                    }

                    var aResultParameter = [];
                    for(var i = 0; i < aURLParameter.length; i++)
                    {
                        if(aURLParameter[i].match(/\=/i) !== null)
                        {
                            var aParameter = aURLParameter[i].split("=");

                            // Schutz vor && in URL
                            if(aParameter[0].length > 0) {
                                // BlackListParameter - Parameter nicht tracken
                                if (this.oLocalConfData.aBlackListParameter.indexOf(aParameter[0].toLowerCase()) > -1) {
                                    continue;
                                }

                                // BlackListValue - Key tracken, Value nicht tracken
                                if (this.oLocalConfData.aBlackListValue.indexOf(aParameter[0].toLowerCase()) > -1) {
                                    aResultParameter.push(aParameter[0]);
                                    continue;
                                }

                                // Parameter in eigenen CP Parameter schreiben
                                if (aParameter[0] in this.oLocalConfData.aWhiteListPageParameter === true) {
                                    this.wt.customParameter[this.oLocalConfData.aWhiteListPageParameter[aParameter[0].toLowerCase()]] = aParameter[1];
                                    continue;
                                }

                                // Fallback
                                aResultParameter.push(aParameter[0].toLowerCase() + "=" + aParameter[1]);
                            }
                        }
                    }

                    this.wt.customParameter[sParameterKey] = aResultParameter.join(";");
                }
            }
        },
        "trackEvent": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackEvent"))debugger;

            var sGroup = oArgs.group;
            var sName = oArgs.name;

            var sValue = "";
            if (typeof oArgs.value !== "undefined" && oArgs.value !== null) {
                var sValue = oArgs.value.toString();
            }

            var iNumValue = ""
            if (typeof oArgs.numvalue !== "undefined" && oArgs.numvalue !== null) {
                var iNumValue = oArgs.numvalue.toString();
            }

            var oPar = {};


            var iParameterKeyGroup = this.oLocalConfData["iEventClickParameterKeyGroup"];
            var iParameterKeyValue = this.oLocalConfData["iEventClickParameterKeyValue"];
            var iParameterKeyNumValue = this.oLocalConfData["iEventClickParameterKeyNumValue"];

            this.wt["linkId"] = sName;
            if (typeof this.wt["customClickParameter"] === "undefined") {
                this.wt["customClickParameter"] = new Object;
            }

            this.wt["customClickParameter"][iParameterKeyGroup] = sGroup;
            this.wt["customClickParameter"][iParameterKeyValue] = sValue;
            this.wt["customClickParameter"][iParameterKeyNumValue] = iNumValue;

            for (var sKey in oPar) {
                this.wt["customClickParameter"][sKey] = oPar[sKey];
            }

            this.sendPixel(false);

        },
        "trackRecommendationClick" : function(oArgs)
        {
            if(this.oWrapper.debug("wt","trackRecommendationClick"))debugger;

            if(typeof oArgs !== "undefined") {
                var oPush = {
                    "group": "recommendationClick_" + oArgs.provider,
                    "name": oArgs.sliderid,
                    "value": oArgs.productid
                };

                this.trackEvent(oPush);
            }
        },
        "trackError": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackError"))debugger;

            // Virutelle URL bei Event-Tracken
            var iParameterOutLink = this.oLocalConfData["iEventClickParameterOutLink"];

            if(typeof this.wt["customClickParameter"] === "undefined")
            {
                this.wt["customClickParameter"] = new Object;
            }

            this.wt["customClickParameter"][iParameterOutLink] = this.sVirtualUrl;

            this.trackEvent({
                "group" :   "Error Messages",
                "name"  :   oArgs.message,
                "value" :   oArgs.source
            });
        },
        "trackSearch": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackSearch"))debugger;

            var sSearchGroup = oArgs.group;
            var sSearchString = oArgs.term;
            var sSearchSource = oArgs.source;
            var sSearchType = oArgs.type;
            var sSearchGoal = oArgs.goal;
            var iSearchResult = oArgs.countresults;

            var iPageParKey = this.oLocalConfData["iSearchPageParameterKey"];
            var aCluster = this.oLocalConfData["oSearchCountCluster"];
            var sCluster = false;
            for (var i = 0; i < aCluster.length; i++)
            {
                if (aCluster[i]["min"] <= iSearchResult && aCluster[i]["max"] >= iSearchResult)
                {
                    sCluster = aCluster[i]["min"] + "-" + aCluster[i]["max"];
                    break;
                }
            }
            if (sCluster == false)
            {
                sCluster = aCluster[aCluster.length - 1]["max"] + " +";
            }

            this.wt.internalSearch = sSearchString;

            if (typeof this.wt.customParameter === "undefined")
            {
                this.wt.customParameter = new Object;
            }

            this.wt.customParameter[iPageParKey] = sCluster;

            //Search Tracking

            var iParameterKeySearchSource = this.oLocalConfData["iEventClickParameterKeySearchSource"];
            var iParameterKeySearchType = this.oLocalConfData["iEventClickParameterKeySearchType"];
            var iParameterKeySearchGoal = this.oLocalConfData["iEventClickParameterKeySearchGoal"];
            var iParameterKeySearchTerm = this.oLocalConfData["iEventClickParameterKeySearchTerm"];
            var iParameterKeySearchCountresults = this.oLocalConfData["iEventClickParameterKeySearchCountresults"];


            if(typeof this.wt["customClickParameter"] === "undefined")
            {
                this.wt["customClickParameter"] = new Object;
            }

            this.wt["customClickParameter"][iParameterKeySearchSource] = sSearchSource;
            this.wt["customClickParameter"][iParameterKeySearchType] = sSearchType;
            this.wt["customClickParameter"][iParameterKeySearchGoal] = sSearchGoal;
            this.wt["customClickParameter"][iParameterKeySearchTerm] = sSearchString;
            this.wt["customClickParameter"][iParameterKeySearchCountresults] = iSearchResult.toString();


            // ------------------ OPTIONAL Search Filter Tracking ----------------------------
            var oSearchFilter = oArgs.filter;
            var oParameterKeySearchFilter = this.oLocalConfData["oEventClickParameterFilter"];

            // Check if Filter Object exists AND Filter Webtrekk Parameter exists in Config
            if (typeof oSearchFilter !== "undefined" && typeof oParameterKeySearchFilter !== "undefined") {
              // Search in Config Object which Filters are defined
              for (var key in oParameterKeySearchFilter) {
                // Check if Search Filter also exists in oArgs.filter Object
                if (typeof oSearchFilter[key] !== "undefined") {
                  var filterParam = oParameterKeySearchFilter[key];
                  this.wt.customParameter[filterParam] = oSearchFilter[key];
                }
              }
            }
            // END --------------- OPTIONAL Search Filter Tracking ----------------------------


            //Search Events tracken
            this.trackEvent({
                "group" : "search",
                "name" : oArgs.source
            });



            // Pagevars tracken

            if(typeof oArgs.source !== "undefined")
            {
                this.trackPageVar({key : "searchsource", value : oArgs.source});
            }

            if(typeof oArgs.type !== "undefined")
            {
                this.trackPageVar({key : "searchtype", value : oArgs.type});
            }

            if(typeof oArgs.goal !== "undefined")
            {
                this.trackPageVar({key : "searchgoal", value : oArgs.goal});
            }

            if(typeof oArgs.countresults !== "undefined")
            {
                this.trackPageVar({key : "searchcountresults", value : (oArgs.countresults).toString()});
                /*
                 Wenn V5 Pixel umgestellt ist, soll countresults direkt an WT gesendet werden
                 this.wt.numberSearchResults = (oArgs.countresults).toString();
                 */
            }

        },
        "trackTeaserView": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackTeaserView"))debugger;

            var sTeaserName = oArgs.name;

            this.trackEvent({group:"teaserView", name:sTeaserName});
        },
        "trackTeaser": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackTeaser"))debugger;

            var sTeasername = oArgs.name;

            var sMediaCode = this.oLocalConfData["sTeaserMediaCode"];
            this.wt.campaignId = sMediaCode + "=" + sTeasername;

            this.trackEvent({group:"teaserClick", name:sTeasername});
        },
        "trackForm": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackForm"))debugger;

            var sFormID = oArgs.formid;

            this.wt.formTrackInstall(document.getElementById(sFormID));
        },
        "trackPageVar": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackPageVar"))debugger;

            var sPageKey = oArgs.key;
            var sPageValue = oArgs.value;


            var sParameterKey = this.oLocalConfData["aPageVarKeys"][sPageKey];
            if (typeof this.wt.customParameter === "undefined")
            {
                this.wt.customParameter = {};
            }
            this.wt.customParameter[sParameterKey] = sPageValue;

            // Zwischenspeichern aller PageVars für weitere Zugriffe
            this.aPageVarsPVCache[sParameterKey] = sPageValue;
        },
        "trackECommerceVar": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackECommerceVar"))debugger;

            var lowercaseParameter = this.oLocalConfData.ProductEcommerceLowercaseKeys;

            if(typeof this.oLocalConfData.aProductEcommerceVarKeys !== "undefined" && typeof this.oLocalConfData.aProductEcommerceVarKeys[oArgs.key] !== "undefined"){
                if(lowercaseParameter.includes(oArgs.key)){
                    this.oCacheEcommerce[oArgs.key] = oArgs.value.toLowerCase();
                } else{
                    this.oCacheEcommerce[oArgs.key] = oArgs.value;
                }
            }else{
                if(lowercaseParameter.includes(oArgs.key)){
                    var sValue = oArgs.value.toLowerCase();
                } else{
                    var sValue = oArgs.value;
                }
                var sKey = oArgs.key;


                var sParameterKey = this.oLocalConfData["aECommerceVarKeys"][sKey];
                if (typeof this.wt.customEcommerceParameter === "undefined")
                {
                    this.wt.customEcommerceParameter = {};
                }
                this.wt.customEcommerceParameter[sParameterKey] = sValue;
            }
        },
        "trackContentGroup": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackContentGroup"))debugger;

            var sContentKey = oArgs.key;
            var sContentValue = oArgs.value;

            if(
                typeof this.oLocalConfData["aContentGroup"] !== "undefined" &&
                typeof this.oLocalConfData["aContentGroup"][sContentKey] !== "undefined"
            )
            {
                var sParameterKey = this.oLocalConfData["aContentGroup"][sContentKey];
                if (typeof this.wt.contentGroup === "undefined")
                {
                    this.wt.contentGroup = {};
                }
                this.wt.contentGroup[sParameterKey] = sContentValue;
            }
        },
        "trackSessionVar": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackSessionVar"))debugger;

            var sSessionKey = oArgs.key;
            var sSessionValue = oArgs.value;

            var sParameterKey = this.oLocalConfData["aSessionVarKeys"][sSessionKey];
            if (typeof this.wt.customSessionParameter === "undefined")
            {
                this.wt.customSessionParameter = {};
            }
            this.wt.customSessionParameter[sParameterKey] = sSessionValue;
        },
        "trackUserVar": function (oArgs)
        {
            if(this.oWrapper.debug("wt","trackUserVar"))debugger;

            var sUserKey = oArgs.key;
            var sUserValue = oArgs.value;

            var sParameterKey = this.oLocalConfData["aUserSessionVarKeys"][sUserKey];
            if (typeof this.wt.customSessionParameter === "undefined")
            {
                this.wt.customSessionParameter = {};
            }
            this.wt.customSessionParameter[sParameterKey] = sUserValue;

            //HIER EIN COOKIE SETZEN UND DIESEN WERT DAUERHAFT SPEICHERN.
            var sCookie = this.oWrapper.readCookie("w18_tg_wt_uservar");
            var oCookie = JSON.parse(sCookie);
            if(oCookie === null)
            {
                oCookie = {};
            }
            oCookie[sUserKey] = sUserValue;
            sCookie = JSON.stringify(oCookie);
            this.oWrapper.setCookie("w18_tg_wt_uservar",sCookie,365);
        },
        // PageParameterTracking
        "trackPageParameter" : function(oPageParameter) {

            if (typeof oPageParameter == "object")
            {
                // Wenn altes Objekt => 1:Produkte, dann retour versetzen 2-6 auf 1-5
                if(typeof oPageParameter[1] !== "undefined" && oPageParameter[1] === "Produkte")
                {
                    for(var i = 2; i <= 6; i++)
                    {
                        if(oPageParameter.hasOwnProperty(i))
                        {
                            oPageParameter[i-1] = oPageParameter[i];
                        }
                    }
                }

                var aGroupConfig = this.oLocalConfData.aContentGroupVariable;

                //Zwischenspeicher zurücksetzen
                this.aPageVarsCache = {};

                for (var sWtVarKey in aGroupConfig)
                {
                    var aGroupIds = aGroupConfig[sWtVarKey];
                    var sWtVarValue = "";
                    for (var i = 0; i < aGroupIds.length; i++)
                    {
                        var iGrouDataKey = aGroupIds[i];
                        if (typeof oPageParameter[iGrouDataKey] !== "undefined" && oPageParameter[iGrouDataKey] !== "")
                        {
                            sWtVarValue += oPageParameter[iGrouDataKey] + ";";
                        }
                    }
                    if (sWtVarValue !== "")
                    {
                        sWtVarValue = sWtVarValue.substring(0, sWtVarValue.length - 1);
                        this.trackPageVar({key:sWtVarKey, value:sWtVarValue});
                    }
                }
            }

        },
        "trackUserData" : function(oArgs)
        {
            if(this.oWrapper.debug("wt","trackUserData"))debugger;

            if (typeof oArgs.isloggedin === "boolean")
            {
                this.trackPageVar({
                    key: "loginstatus",
                    value: oArgs.isloggedin === true ? "Angemeldet" : "Anonym"
                });
            }

            else
            {
                this.trackPageVar({
                    key: "loginstatus",
                    value: typeof oArgs.isloggedin + ":" + oArgs.isloggedin
                });
            }
        },
        "cookieweicheConversion" : function(oArgs)
        {
            if(this.oWrapper.debug("wt","cookieweicheConversion"))debugger;

            if(oArgs.partnerIds.length > 0)
            {
                this.sCWPartner = oArgs.partnerIds.join(";");
            }
            else
            {
                this.sCWPartner = "-";
            }

            this.sendCookieWeicheAttributionEvent();
        },
        "sendCookieWeicheAttributionEvent" :function()
        {
            if(this.oWrapper.debug("wt","sendCookieWeicheAttributionEvent"))debugger;

            if(this.sOrderID !== false && this.sCWPartner !== false)
            {
                this.trackEvent({
                    group   :   "Cookieweiche_Attribution",
                    name    :   this.sOrderID,
                    value   :   this.sCWPartner
                });
            }
        }
    });

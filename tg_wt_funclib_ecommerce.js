
tgNamespace.tgContainer.registerInnerFuncLib(
    "wt", {

        "addToCart": function (oArgs) {
            if (this.oWrapper.debug("wt", "addToCart"))debugger;

            var aProducts = this.getProductsFromType("addtocart");

            var iLastIndex = aProducts.length - 1;

            var sSku = aProducts[0].id;
            var sProductName = aProducts[0].name;
            var iProuctQuantity = aProducts[0].quantity;
            var sProductCost = aProducts[0].brutto;
            var oProductGroup = aProducts[0].group;
            var sOldBrutto = aProducts[0].oldbrutto;
            var aMarke = aProducts[0].brand;

            if(typeof aMarke === "string")
            {
                aMarke = [aMarke];
            }

            this.emptyProductsFromType("addtocart");

            //Add2Cart mit einem normalen Pageview absetzen

            // PageVars aus Zwischenspeicher mitübergeben
            if (typeof oProductGroup === "undefined") {
                if (typeof this.wt.customParameter === "undefined") {
                    this.wt.customParameter = {};
                }

                for (var key in this.aPageVarsCache) {
                    this.wt.customParameter[key] = this.aPageVarsCache[key];
                }
            } else {
                this.trackPageParameter(oProductGroup);
            }


            if (typeof this.wt.product != "undefined") {
                this.wt.product = this.wt.product + ";" + sSku;
            }
            else {
                this.wt.product = sSku;
            }
            if (typeof this.wt.productQuantity != "undefined") {
                this.wt.productQuantity = this.wt.productQuantity + ";" + iProuctQuantity;
            }
            else {
                this.wt.productQuantity = iProuctQuantity;
            }
            if (typeof this.wt.productCost != "undefined") {
                this.wt.productCost = this.wt.productCost + ";" + sProductCost * iProuctQuantity;
            }
            else {
                this.wt.productCost = sProductCost * iProuctQuantity;
            }
            this.wt.productStatus = "add";
            var iEcommerceParameterProductName = this.oLocalConfData["iEcommerceParameterProductViewProductName"];
            if (iEcommerceParameterProductName != false) {
                if (typeof this.wt.customEcommerceParameter == "undefined") {
                    this.wt.customEcommerceParameter = {};
                }
                this.wt.customEcommerceParameter[iEcommerceParameterProductName] = sProductName;
            }

            var iEcommerceParameterProductOldbrutto = this.oLocalConfData["iEcommerceParameterProductOldBrutto"];
            if (iEcommerceParameterProductOldbrutto != false) {
                if (typeof this.wt.customEcommerceParameter == "undefined") {
                    this.wt.customEcommerceParameter = {};
                }
                this.wt.customEcommerceParameter[iEcommerceParameterProductOldbrutto] = sOldBrutto;
            }

            if(typeof aMarke !== "undefined" && aMarke.length > 0)
            {
                var iEcommerceParameterProductBrand = this.oLocalConfData["iEcommerceParameterProductBrand"];
                if (iEcommerceParameterProductBrand != false) {
                    if (typeof this.wt.customEcommerceParameter == "undefined") {
                        this.wt.customEcommerceParameter = {};
                    }
                    this.wt.customEcommerceParameter[iEcommerceParameterProductBrand] = aMarke.join(";");
                }
            }

            this.sendPixel();
        },

        "deleteFromCart": function (oArgs) {
            if (this.oWrapper.debug("wt", "deleteFromCart"))debugger;


            var aProducts = this.getProductsFromType("deletefromcart");


            var sSku = aProducts[0].id;
            var sProductName = aProducts[0].name;
            var iProductQuantity = 0;
            var sProductCost = aProducts[0].brutto;
            var oProductGroup = aProducts[0].group;
            var sOldBrutto = aProducts[0].oldbrutto;
            var aMarke = aProducts[0].brand;

            /*if(typeof aMarke === "string")
             {
             aMarke = [aMarke];
             }*/

            this.emptyProductsFromType("deletefromcart");

            //Add2Cart mit einem normalen Pageview absetzen

            // PageVars aus Zwischenspeicher mitübergeben
            /*if (typeof oProductGroup === "undefined") {
             if (typeof this.wt.customParameter === "undefined") {
             this.wt.customParameter = {};
             }

             for (key in this.aPageVarsCache) {
             this.wt.customParameter[key] = this.aPageVarsCache[key];
             }
             } else {
             this.trackPageParameter(oProductGroup);
             }*/


            if (typeof this.wt.product != "undefined") {
                this.wt.product = this.wt.product + ";" + sSku;
            }
            else {
                this.wt.product = sSku;
            }
            if (typeof this.wt.productQuantity != "undefined") {
                this.wt.productQuantity = this.wt.productQuantity + ";" + iProductQuantity;
            }
            else {
                this.wt.productQuantity = iProductQuantity;
            }
            /* if (typeof this.wt.productCost != "undefined") {
             this.wt.productCost = this.wt.productCost + ";" + sProductCost * iProductQuantity;
             }
             else {
             this.wt.productCost = sProductCost * iProductQuantity;
             } */
            this.wt.productStatus = "add";
            /*var iEcommerceParameterProductName = this.oLocalConfData["iEcommerceParameterProductViewProductName"];
             if (iEcommerceParameterProductName != false) {
             if (typeof this.wt.customEcommerceParameter == "undefined") {
             this.wt.customEcommerceParameter = {};
             }
             this.wt.customEcommerceParameter[iEcommerceParameterProductName] = sProductName;
             }

             var iEcommerceParameterProductOldbrutto = this.oLocalConfData["iEcommerceParameterProductOldBrutto"];
             if (iEcommerceParameterProductOldbrutto != false) {
             if (typeof this.wt.customEcommerceParameter == "undefined") {
             this.wt.customEcommerceParameter = {};
             }
             this.wt.customEcommerceParameter[iEcommerceParameterProductOldbrutto] = sOldBrutto;
             }

             if(typeof aMarke !== "undefined" && aMarke.length > 0)
             {
             var iEcommerceParameterProductBrand = this.oLocalConfData["iEcommerceParameterProductBrand"];
             if (iEcommerceParameterProductBrand != false) {
             if (typeof this.wt.customEcommerceParameter == "undefined") {
             this.wt.customEcommerceParameter = {};
             }
             this.wt.customEcommerceParameter[iEcommerceParameterProductBrand] = aMarke.join(";");
             }
             } */
            var sParameterKey = this.oLocalConfData["aECommerceVarKeys"]["deletefromcart"];
            if (typeof this.wt.customEcommerceParameter === "undefined" && typeof sParameterKey !== "undefined")
            {
                this.wt.customEcommerceParameter = {};
            }
            this.wt.customEcommerceParameter[sParameterKey] = "Delete from Cart";

            this.sendPixel();



        },


        "trackproduct": function (oArgs) {
            if (this.oWrapper.debug("wt", "trackproduct"))debugger;

            if(typeof oArgs.type === "undefined")
            {
                oArgs.type = "default";
            }

            if(typeof this.oProducts[oArgs.type] === "undefined")
            {
                this.oProducts[oArgs.type] = [];
            }

            if(Object.keys(this.oCacheEcommerce).length > 0)
            {
                this.oProductEcommerce[oArgs.id] = this.oCacheEcommerce;
                this.oCacheEcommerce = {};
            }

            this.oProducts[oArgs.type].push(oArgs);

        },
        "getProductsFromType" : function (sType)
        {
            if(typeof this.oProducts[sType] === "object")
            {
                return this.oProducts[sType];
            }
            else if(typeof this.oProducts["default"] === "object")
            {
                return this.oProducts["default"];
            }
            else
            {
                return [];
            }
        },
        "emptyProductsFromType" : function (sType)
        {
            if(typeof this.oProducts[sType] === "object")
            {
                this.oProducts[sType] = [];
            }
            else if(typeof this.oProducts["default"] === "object")
            {
                this.oProducts["default"] = [];
            }
        },
        //diese Funktion wird nicht mehr von außen, nur noch von innen aufgerufen.
        "INNERtrackSaleProduct": function (sOrderID, oArgs) {
            var sProductID = oArgs.id;
            var oProductGroup = oArgs.group;
            var sProductName = oArgs.name;
            var iProductQuantity = oArgs.quantity;
            var sProductCost = oArgs.brutto;
            var oPar = {};

            if (typeof oPar === "object") {
                if (typeof this.wt.customEcommerceParameter == "undefined") {
                    this.wt.customEcommerceParameter = oPar;
                }
                else {
                    for (var sKey in oPar) {
                        this.wt.customEcommerceParameter[sKey] = oPar[sKey];
                    }
                }
            }

            if (typeof this.wt.product != "undefined") {
                this.wt.product = this.wt.product + ";" + sProductID;
            }
            else {
                this.wt.product = sProductID;
            }
            if (typeof this.wt.productQuantity != "undefined") {
                this.wt.productQuantity = this.wt.productQuantity + ";" + iProductQuantity;
            }
            else {
                this.wt.productQuantity = iProductQuantity;
            }
            if (typeof this.wt.productCost != "undefined") {
                this.wt.productCost = this.wt.productCost + ";" + sProductCost * iProductQuantity;
            }
            else {
                this.wt.productCost = (sProductCost * iProductQuantity).toString();
            }

            this.wt.productStatus = "conf";
            var iEcommerceParameterProductName = this.oLocalConfData["iEcommerceParameterProductViewProductName"];
            if (iEcommerceParameterProductName != false) {
                if (typeof this.wt.customEcommerceParameter == "undefined") {
                    this.wt.customEcommerceParameter = {};
                }
                this.wt.customEcommerceParameter[iEcommerceParameterProductName] = sProductName;
            }

            var oProductEcommerce = this.oProductEcommerce[sProductID.toString()]; //Produkt
            var bOnlineOccured = false;

            for (var sEcEntry in oProductEcommerce) {

                if (sEcEntry === "isOnlineOnly" && typeof oProductEcommerce[sEcEntry] === "boolean") {
                    if (oProductEcommerce[sEcEntry] === true){
                        (typeof this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] === "undefined") ? this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = "ja" :
                            this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] + ";" + "ja";
                    }
                    else{
                        (typeof this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] === "undefined") ? this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = "nein" :
                            this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] + ";" + "nein";
                    }
                    bOnlineOccured = true;
                }
                else if (sEcEntry === "isOnlineOnly" && typeof oProductEcommerce[sEcEntry] !== "boolean") {
                    var sResult = typeof oProductEcommerce[sEcEntry] + " : " + oProductEcommerce[sEcEntry];

                    (typeof this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] === "undefined") ? this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = sResult :
                        this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] + ";" + sResult;
                    bOnlineOccured = true;
                }
            }

            if(!bOnlineOccured){
                (typeof this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys["isOnlineOnly"])] === "undefined") ? this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys["isOnlineOnly"])] = "not found" :
                    this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys["isOnlineOnly"])] = this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys["isOnlineOnly"])] + ";" + "not found";
            }

            oProductEcommerce = {};
        },

        "trackSale": function (oArgs) {

            if (this.oWrapper.debug("wt", "trackSale"))debugger;

            var sOrderID = oArgs.orderid;
            var sTotal = oArgs.brutto;
            var sTax = oArgs.tax;
            var sShipping = oArgs.shipping;
            var sCity = oArgs.city;
            var sState = oArgs.state;
            var sCountry = oArgs.country;
            var sZahlart = oArgs.paytype;
            var sVouchercode = oArgs.vouchercode;
            var sVouchervalue = oArgs.vouchervalue;
            var sCurrency = oArgs.currency;
            var oVouchers = oArgs.vouchers;

            if(typeof this.oLocalConfData.bS2STracking !== "undefined" && this.oLocalConfData.bS2STracking === true)
            {
                this.sOrderID = sOrderID;
                this.sendCookieWeicheAttributionEvent();
            }
            else
            {
                var oPar = {};

                var oProductGroups = {};
                var sProductBrand = "";

                var aProducts = this.getProductsFromType("sale");

                for (var i = 0; i < aProducts.length; i++) {
                    this.INNERtrackSaleProduct(sOrderID, aProducts[i]);

                    if (typeof aProducts[i].group !== "undefined")
                    {
                        for (var iGroupID in aProducts[i].group) {
                            if(typeof oProductGroups[iGroupID] === "undefined")
                            {
                                oProductGroups[iGroupID] = (aProducts[i].group[iGroupID]);
                            } else {
                                oProductGroups[iGroupID] += (";" + aProducts[i].group[iGroupID]);
                            }
                        }
                    }

                    if(typeof aProducts[i].brand !== "undefined" && aProducts[i].brand.length > 0)
                    {
                        if(sProductBrand === "")
                        {
                            sProductBrand = aProducts[i].brand[0];
                        }
                        else
                        {
                            sProductBrand += (";" + aProducts[i].brand[0]);
                        }
                    }
                }

                if(sProductBrand !== "")
                {
                    var iEcommerceParameterProductBrand = this.oLocalConfData["iEcommerceParameterProductBrand"];
                    if (iEcommerceParameterProductBrand != false) {
                        if (typeof this.wt.customEcommerceParameter == "undefined") {
                            this.wt.customEcommerceParameter = {};
                        }
                        this.wt.customEcommerceParameter[iEcommerceParameterProductBrand] = sProductBrand;
                    }
                }

                this.emptyProductsFromType("sale");
                this.oProductEcommerce = {};

                this.trackProductGroup2Variable(oProductGroups);

                this.wt.orderValue = sTotal;
                this.wt.orderId = sOrderID;

                if (typeof this.wt.customEcommerceParameter == "undefined") {
                    this.wt.customEcommerceParameter = {};
                }

                if (typeof sZahlart !== "undefined" && typeof this.oLocalConfData["aECommerceVarKeys"]["zahlart"] !== "undefined") {
                    this.wt.customEcommerceParameter[this.oLocalConfData["aECommerceVarKeys"]["zahlart"]] = sZahlart;
                }

                if(typeof oVouchers === "object")
                {
                    /**
                     * Desktop läuft über TG4 und hat ein Array aus Voucher-Objekten zur Verfügung
                     */
                    if(oVouchers.length > 0)
                    {
                        var oVoucher = oVouchers[0];

                        if (typeof oVoucher.code !== "undefined" && typeof this.oLocalConfData["aECommerceVarKeys"]["vouchercode"] !== "undefined") {
                            this.wt.customEcommerceParameter[this.oLocalConfData["aECommerceVarKeys"]["vouchercode"]] = oVoucher.code;
                        }

                        if (typeof oVoucher.value !== "undefined" && typeof this.oLocalConfData["aECommerceVarKeys"]["vouchervalue"] !== "undefined") {
                            this.wt.customEcommerceParameter[this.oLocalConfData["aECommerceVarKeys"]["vouchervalue"]] = oVoucher.value;
                        }
                    }
                }
                else
                {
                    /**
                     * Mobile + APP läuft noch über alte TG-Aufrufe
                     */

                    if (typeof sVouchercode !== "undefined" && typeof this.oLocalConfData["aECommerceVarKeys"]["vouchercode"] !== "undefined") {
                        this.wt.customEcommerceParameter[this.oLocalConfData["aECommerceVarKeys"]["vouchercode"]] = sVouchercode;
                    }

                    if (typeof sVouchervalue !== "undefined" && typeof this.oLocalConfData["aECommerceVarKeys"]["vouchervalue"] !== "undefined") {
                        this.wt.customEcommerceParameter[this.oLocalConfData["aECommerceVarKeys"]["vouchervalue"]] = sVouchervalue;
                    }
                }



                // currency ist derzeit NICHT in der Konfiguration gesetzt und wird dadurch nie ausgespielt
                if (typeof sCurrency !== "undefined" && typeof this.oLocalConfData["aECommerceVarKeys"]["currency"] !== "undefined") {
                    this.wt.customEcommerceParameter[this.oLocalConfData["aECommerceVarKeys"]["currency"]] = sCurrency;
                }
            }
        },

        "trackProductView": function () {

            if (this.oWrapper.debug("wt", "trackProductView"))debugger;

            var aProducts = this.getProductsFromType("productview");

            var sProductID = aProducts[0].id;
            var aProductGroup = aProducts[0].group;
            var sProductName = aProducts[0].name;
            var sProduktpreis = aProducts[0].brutto;
            var sOldBrutto = aProducts[0].oldbrutto;
            var aMarke = aProducts[0].brand;

            this.emptyProductsFromType("productview");

            var iEcommerceParameterProductOldbrutto = this.oLocalConfData["iEcommerceParameterProductOldBrutto"];
            if (iEcommerceParameterProductOldbrutto != false) {
                if (typeof this.wt.customEcommerceParameter == "undefined") {
                    this.wt.customEcommerceParameter = {};
                }
                this.wt.customEcommerceParameter[iEcommerceParameterProductOldbrutto] = sOldBrutto;
            }

            this.trackPageParameter(aProductGroup);

            if (typeof this.wt.product != "undefined") {
                this.wt.product = this.wt.product + ";" + sProductID;
            }
            else {
                this.wt.product = sProductID;
            }

            if (typeof this.wt.productCost != "undefined") {
                this.wt.productCost = this.wt.productCost + ";" + sProduktpreis;
            }
            else {
                this.wt.productCost = sProduktpreis;
            }

            this.wt.productStatus = "view";
            var iEcommerceParameterProductName = this.oLocalConfData["iEcommerceParameterProductViewProductName"];
            if (iEcommerceParameterProductName != false) {
                if (typeof this.wt.customEcommerceParameter == "undefined") {
                    this.wt.customEcommerceParameter = {};
                }
                this.wt.customEcommerceParameter[iEcommerceParameterProductName] = sProductName;
            }

            if(typeof aMarke !== "undefined")
            {
                if(typeof aMarke === "string")
                {
                    var sMarke = aMarke;
                }
                else if(typeof aMarke === "Object" && aMarke.length > 0)
                {
                    var sMarke = aMarke.join(";");
                }


                var iEcommerceParameterProductBrand = this.oLocalConfData["iEcommerceParameterProductBrand"];
                if (iEcommerceParameterProductBrand != false) {
                    if (typeof this.wt.customEcommerceParameter == "undefined") {
                        this.wt.customEcommerceParameter = {};
                    }
                    this.wt.customEcommerceParameter[iEcommerceParameterProductBrand] = sMarke;
                }
            }


            var oProductEcommerce = this.oProductEcommerce[sProductID.toString()];
            var bOnlineOccured = false;

            if (typeof this.wt.customEcommerceParameter == "undefined") {
                this.wt.customEcommerceParameter = {};
            }

            for(var sEcEntry in oProductEcommerce){

                if (sEcEntry === "isOnlineOnly" && typeof oProductEcommerce[sEcEntry] === "boolean")
                {
                    (oProductEcommerce[sEcEntry] === true) ? this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = "ja" : this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = "nein";
                    bOnlineOccured = true;
                }
                if (sEcEntry === "isOnlineOnly" && typeof oProductEcommerce[sEcEntry] !== "boolean")
                {
                    this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys[sEcEntry])] = typeof oProductEcommerce[sEcEntry] + " : " + oProductEcommerce[sEcEntry];
                    bOnlineOccured = true;
                }
            }

            if(!bOnlineOccured){
                this.wt.customEcommerceParameter[(this.oLocalConfData.aProductEcommerceVarKeys["isOnlineOnly"])] = "not found";
            }

            this.oProductEcommerce = {};
            oProductEcommerce = {};

            // Für PageVar Zwischenspeicher bei Submit() wichtig
            this.bProductView = true;

        },
        "trackProductGroup2Variable": function (oGroupParameter) {
            if (this.oWrapper.debug("wt", "trackProductGroup2Variable"))debugger;
            if (typeof oGroupParameter === "object") {
                var aGroupConfig = this.oLocalConfData.aProductGroupVariable;

                for (var sGroupDataKey in aGroupConfig) {
                    var aGroupIDs = aGroupConfig[sGroupDataKey];
                    var sWtVarValue = "";

                    for (var iGroupIDs in aGroupIDs) {
                        var iPageID = aGroupIDs[iGroupIDs];
                        if (typeof oGroupParameter[iPageID] !== "undefined" && oGroupParameter[iPageID] !== "") {
                            sWtVarValue += oGroupParameter[iPageID] + ";";
                        }
                    }
                    if (sWtVarValue !== "") {
                        sWtVarValue = sWtVarValue.substring(0, sWtVarValue.length - 1);
                        this.trackECommerceVar({key: sGroupDataKey, value: sWtVarValue});
                    }
                }
            }
        }
    }
);

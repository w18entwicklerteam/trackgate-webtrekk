tgNamespace.tgContainer.registerInnerConfig(
    "wt",
    //LIVE CONF
    {
        //loadPluginFunction: "wt_teaserTracking;wt_marketingAutomation",
        oWebtrekkConfig: {
            trackId: "551478456935721",
            trackDomain: "xxxlmoebelhaeuserde01.wt-eu02.net",
            domain: "REGEXP:^(.*xxxlutz\\.at)|(.*sofort\\.com)|(.*wirecard\\.com)|(.*designatweb\\.eu)$"
        }
    },
    {
        //TEST CONF
        oWebtrekkConfig: {
            trackId: "590844243128338",
            domain: "REGEXP:^(.*xxxlutz\\.at)|(.*sofort\\.com)|(.*wirecard\\.com)|(.*designatweb\\.eu)$"
        }
    }
);

